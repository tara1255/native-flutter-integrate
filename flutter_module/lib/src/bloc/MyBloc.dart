import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'MyEvent.dart';
import 'MyState.dart';

class MyBloc extends Bloc<MyEvent, MyState> {
  @override
  MyState get initialState => SubmitInitial();

  @override
  Stream<MyState> mapEventToState(MyEvent event) async* {
    if (event is SubmitButtonPressed) {
      yield SubmitOnProgress();

      try {
        await submit();
        yield SubmitSuccess();
      } catch (e) {
        yield SubmitFailure();
      }
    }
  }

  Future<void> submit() async {
    var dio = Dio();
    Response response = await dio.get('https://google.com');
    print(response.data);
    // await Future.delayed(Duration(seconds: 2));
    return;
  }
}
