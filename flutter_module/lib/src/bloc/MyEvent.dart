import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class MyEvent extends Equatable {
  const MyEvent();
}

class SubmitButtonPressed extends MyEvent {
  final String starRating;
  final String emotionRating;
  final String suggestion;

  SubmitButtonPressed({
    @required this.starRating,
    @required this.emotionRating,
    this.suggestion,
  });

  @override
  List<Object> get props => [starRating, emotionRating, suggestion];

  @override
  String toString() => 'SubmitButtonPressed!';
}
