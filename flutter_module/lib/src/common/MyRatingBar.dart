import 'package:flutter/material.dart';
import 'package:fluttermodule/src/common/MyColor.dart';

class MyRatingBar extends StatelessWidget {
  final value;
  final itemCount;
  final Function(int) onRatingUpdate;

  MyRatingBar({@required this.value,@required this.itemCount, this.onRatingUpdate});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        child: SizedBox(
          height: 40,
          child: ListView.builder(
            shrinkWrap: true,
            itemCount: itemCount,
            scrollDirection: Axis.horizontal,
            itemBuilder: (_, i) => GestureDetector(
              onTap: () {
                onRatingUpdate(i+1);
              },
              child: Container(
                height: 40,
                width: 40,
                margin: EdgeInsets.only(left: 6, right: 6),
                child: Image.asset(
                  "assets/images/star_filled.png",
                  color: (i+1 <= value) ? null : MyColor.grey,
                ),
              ),
            ),
          ),
          // child: Text("data22"),
        ),
      ),
    );
  }
}
