import 'package:flutter/material.dart';

class MyColor {
  static const green = Color(0xFF48A800); //rgb 72 168 0
  static const grey = Color(0xFFd7d7d7); //rgb 109 114 120
  static const white = Color(0xFFFFFFFF); //rgb 255 255 255
  static const blackText1 = Color(0xFF535353); //rgb 37 40 43
  static const blackText2 = Color(0xFF868686); //rgb 66 66 66
  static const blueAppBar = Color(0xFF0179c4);
  static const blueBody = Color(0xffeff4f6);
  static const blueBorder = Color(0xff1d7eb5);
  static const blueButton = Color(0xff0087c6);
}
