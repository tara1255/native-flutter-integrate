import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'src/bloc/MyBloc.dart';
import 'src/ui/MainActivity.dart';

void main() {
  runApp(MaterialApp(
    title: "Tunaiku",
    home: BlocProvider(
      create: (BuildContext context) => MyBloc(),
      child: MainActivity(),
    ),
    debugShowCheckedModeBanner: false,
  ));
}
