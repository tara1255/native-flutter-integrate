package com.example.mergeflutter

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.embedding.engine.FlutterEngineCache
import io.flutter.embedding.engine.dart.DartExecutor
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
//    private val FULL_SCREEN_ENGINE_ID = "fullScreenEngineId"
    private val CHANNEL = "samples.flutter.dev/tunaiku"
    private var flutterEngine: FlutterEngine? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        btnFlutter.setOnClickListener {
            startActivity(
                FlutterActivity
//                    .withNewEngine()
                    .withCachedEngine(FULL_SCREEN_ENGINE_ID)
                    .build(this)
            )
        }

        setUpFlutter()

    }

    private fun setUpFlutter() {

        if (flutterEngine == null) {
            flutterEngine = FlutterEngineCache.getInstance().get(FULL_SCREEN_ENGINE_ID)
            flutterEngine!!
                .dartExecutor
                .executeDartEntrypoint(
                    DartExecutor.DartEntrypoint.createDefault()
                )
        }

//        GeneratedPluginRegistrant.registerWith(flutterEngine!!)

        MethodChannel(flutterEngine!!.dartExecutor.binaryMessenger, CHANNEL)
            .setMethodCallHandler { call, result ->
                run {
                    val arguments: Map<String, Any> =
                        call.arguments()

                    when (call.method) {
                        "getData" -> result.success(getData(arguments["from"]))
                        "finish" ->
                        {
                            Log.d("TAGF3", call.method.toString())
//                            flutterEngine.
//                                    FlutterActivity().finish()
                        }
                        else -> result.notImplemented()
                    }
                }
            }
    }

    private fun getData(s: Any?): Map<String, String> {
        return mapOf("key1" to "Hy $s, Value From Native")
    }


}
